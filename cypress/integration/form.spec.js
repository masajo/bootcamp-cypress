// Creamos nuestro primer SPEC de Cypress para que se pueda ejecutar

// Creamos nuestro primer grupo (describe) de pruebas (it)
describe('Test de nuestro formulario', () => {

    // Aquí dentro van todas las pruebas que se deben ejecutar
    it('Rellenar el formulario', () => {
        
        // Navegamos a la raíz de la web
        cy.visit('/');
        // Obtenemos el formulario, usamos GET para la etiqueta <form></form>
        cy.get('form')

        cy.get('#name').type("Martin").should('have.value', "Martin")
        cy.get('#email').type("martin.imaginagroup.com").should('have.value', "martin.imaginagroup.com")
        cy.get('#message').type("Hola Mundo").should('have.value', "Hola Mundo")

        // Pulsar el botón de submit
        cy.get('form').submit()
    });
    




    }
);