/**
 * Script para buscar el formulario y
 * especificar el evento de SUBMIT que se debe ejecutar
 * al pulsar el botón de ENVIAR (Send)
 */

// Definimos una constante que guarde el elemento FORM
// Obtenemos el formulario a partir de la lista de formularios que hay en el documento
// Todas las listas empiezan en 0, por lo que la posición 0 es el primero
// Dado que solo hay un <form></form> en el index.html, queremos la posición [0]
const form = document.forms[0];

// Vamos a añadirle un EventListener al evento de SUBMIT del formulario
// Es decir, cuando se pulse el botón con tipo submit dentro del formulario
// Se ejecuta la fucnión definida en este punto como EventListener


form.addEventListener("submit", (event) => {
        // Estas son las líneas que se ejecutan cuando un evento submit ocurre
        // Lo primero que debemos hacer para evitar que se recargue la página al hacer el submit
        // que es el comportamiento por defecto
        event.preventDefault();
        console.log('¡Botón SUBMIT PULSADO!')
    }
)